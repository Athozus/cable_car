dofile(minetest.get_modpath("cable_car") .. "/nodes.lua")
dofile(minetest.get_modpath("cable_car") .. "/cable.lua")
dofile(minetest.get_modpath("cable_car") .. "/cabin.lua")
dofile(minetest.get_modpath("cable_car") .. "/tensioner.lua")
